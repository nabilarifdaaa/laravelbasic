<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comment', 'id_user','id_post',
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }
}

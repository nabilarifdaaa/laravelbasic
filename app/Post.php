<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'id_user',
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }

    public function comment(){
        return $this->hasMany(Comment::class, 'id_post', 'id');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $data = Post::get();
        $data = Post::with(['user'])->get();
        // dd($data[0]->user->name);
        return view('home',compact('data'));
    }

    public function input(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        // dd(Auth::user());

        $data = new Post();
        $data->title = $request->title;
        $data->content = $request->content;
        $data->id_user = Auth::user()->id;
        $data->save();
        // dd($data);
        return redirect()->back();
        // return redirect()->route('hasil');
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> {{$post->title}} || {{ $post->user->name }} </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('comment', $post->id) }}">
                            @csrf
                            <div class="form-group row">
                            
                                <div class="col-md-12">
                                    {{$post->content}}
                                    <br><br>
                                </div>


                                <div class="col-md-12">
                                    <input id="comment" type="text" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment" placeholder="Comment" required autofocus>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('comment') }}</strong>
                                        </span>
                                    <br>
                                </div>

                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Comment') }}
                                    </button>
                                </div>
                                @foreach($comment as $data)
                                <div class="col-md-12">
                                    {{ $data->user->name }} ||
                                    <strong>{{ $data->comment }}</strong>

                                </div>
                                @endforeach
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
